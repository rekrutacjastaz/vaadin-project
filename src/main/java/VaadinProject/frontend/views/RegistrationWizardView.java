package VaadinProject.frontend.views;

import org.vaadin.teemu.wizards.Wizard;
import org.vaadin.teemu.wizards.event.WizardCancelledEvent;
import org.vaadin.teemu.wizards.event.WizardCompletedEvent;
import org.vaadin.teemu.wizards.event.WizardProgressListener;
import org.vaadin.teemu.wizards.event.WizardStepActivationEvent;
import org.vaadin.teemu.wizards.event.WizardStepSetChangedEvent;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.ClassResource;
import com.vaadin.server.Page;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Image;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

import VaadinProject.backend.entities.User;
import VaadinProject.backend.services.UserService;
import VaadinProject.frontend.registrationSteps.RegistrationStep1;
import VaadinProject.frontend.registrationSteps.RegistrationStep2;
import VaadinProject.frontend.registrationSteps.RegistrationStep3;
import VaadinProject.frontend.registrationSteps.RegistrationStep4;
import VaadinProject.frontend.registrationSteps.RegistrationStep5;
import VaadinProject.frontend.registrationSteps.RegistrationStep6;

@SuppressWarnings("serial")
public class RegistrationWizardView extends CustomComponent implements WizardProgressListener, View {

	public static final String NAME = "RegistrationWizard";
	public static User newUser = new User();

	private Wizard wizard;
	private VerticalLayout mainLayout;
	
	private Window subWindow;
	private Image roller = new Image(null, new ClassResource("/rolling.gif"));
	private Image tick = new Image(null, new ClassResource("/tick.png"));

	public RegistrationWizardView() {
		buildLayout();
	}

	private void buildLayout() {
		mainLayout = new VerticalLayout();
		mainLayout.setSizeFull();
		mainLayout.setMargin(true);
		setCompositionRoot(mainLayout);

		wizard = new Wizard();
		wizard.setUriFragmentEnabled(true);
		wizard.addListener(this);
		wizard.addStep(new RegistrationStep1(), "intro");
		wizard.addStep(new RegistrationStep2(), "username");
		wizard.addStep(new RegistrationStep3(), "email");
		wizard.addStep(new RegistrationStep4(), "password");
		wizard.addStep(new RegistrationStep5(), "avatar");
		wizard.addStep(new RegistrationStep6(), "terms");
		wizard.setHeight("800px");
		wizard.setWidth("1000px");

		mainLayout.addComponent(wizard);
		mainLayout.setComponentAlignment(wizard, Alignment.TOP_CENTER);

		setCompositionRoot(mainLayout);
	}

	@Override
	public void activeStepChanged(WizardStepActivationEvent event) {
		Page.getCurrent().setTitle(event.getActivatedStep().getCaption());

	}

	@Override
	public void stepSetChanged(WizardStepSetChangedEvent event) {
	}

	@Override
	public void wizardCompleted(WizardCompletedEvent event) {
		UserService userService = UserService.createMockService();
		newUser.setRole(User.Role.USER);
		userService.save(newUser);

		getUI().getNavigator().navigateTo(NotebookListView.NAME);
		Page.getCurrent().setTitle("Notebook warehouse helper");
		showWindow();
	}

	@Override
	public void wizardCancelled(WizardCancelledEvent event) {
		getUI().getNavigator().navigateTo(NotebookListView.NAME);
		Notification.show("Registration cancelled", Type.WARNING_MESSAGE);
	}

	@Override
	public void enter(ViewChangeEvent event) {
	}
	
	private void showWindow() {
		subWindow = new Window();
		subWindow.setClosable(false);
		subWindow.setResizable(false);
		subWindow.addStyleName("transparent");

		VerticalLayout subContent = new VerticalLayout();
		subContent.setMargin(true);

		subWindow.setSizeUndefined();
		subContent.addComponents(roller, tick);
		subContent.setComponentAlignment(roller, Alignment.MIDDLE_CENTER);
		subContent.setComponentAlignment(tick, Alignment.MIDDLE_CENTER);
		tick.setVisible(false);
		subWindow.center();

		subWindow.setContent(subContent);
		UI.getCurrent().addWindow(subWindow);
		new changeToTick().start();
	}
	
	class changeToTick extends Thread {
		@Override
		public void run() {
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {}

			UI.getCurrent().access(new Runnable() {
				@Override
				public void run() {
					roller.setVisible(false);
					tick.setVisible(true);
					new closeWindow().start();
				}
			});
		}
	}
	
	class closeWindow extends Thread {
		@Override
		public void run() {
			try {
				Thread.sleep(2000);

			} catch (InterruptedException e) {
			}

			UI.getCurrent().access(new Runnable() {
				@Override
				public void run() {
					subWindow.close();
				}
			});
		}
	}
}
