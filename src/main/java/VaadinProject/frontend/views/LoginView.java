package VaadinProject.frontend.views;

import com.vaadin.event.ShortcutAction;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.themes.ValoTheme;

import VaadinProject.backend.entities.User;
import VaadinProject.backend.services.UserService;

@SuppressWarnings("serial")
public class LoginView extends CustomComponent implements View {

	public static final String NAME = "Login";

	private final TextField user;
	private final PasswordField password;
	private final Label errorLabel;
	private final Button loginButton;
	private final Button backButton;
	
	public static final String fieldWidth = "300px";
	public static final String buttonWidth = "80px";

	public LoginView() {
		setSizeFull();

		user = new TextField("User:");
		user.setWidth(fieldWidth);
		user.setRequired(true);
		user.setInputPrompt("Your username or email");
		user.focus();

		password = new PasswordField("Password:");
		password.setWidth(fieldWidth);
		password.setRequired(true);
		
		errorLabel =  new Label("Wrong username or password");
		errorLabel.setStyleName(ValoTheme.LABEL_FAILURE);
		errorLabel.setVisible(false);
		
		loginButton = new Button("Login", this::loginButtonClicked);
		loginButton.setStyleName(ValoTheme.BUTTON_PRIMARY);
		loginButton.setClickShortcut(ShortcutAction.KeyCode.ENTER);
		loginButton.setWidth(buttonWidth);
		
		backButton = new Button("Back", e -> getUI().getNavigator().navigateTo(NotebookListView.NAME));
		backButton.setClickShortcut(ShortcutAction.KeyCode.ESCAPE);
		backButton.setWidth(buttonWidth);
		
		HorizontalLayout buttonsLayout = new HorizontalLayout(backButton, loginButton);
		buttonsLayout.setSpacing(true);

		VerticalLayout fields = new VerticalLayout(user, password, errorLabel, buttonsLayout);
		fields.setComponentAlignment(buttonsLayout, Alignment.MIDDLE_CENTER);
		fields.setSpacing(true);
		fields.setSizeUndefined();

		VerticalLayout viewLayout = new VerticalLayout(fields);
		viewLayout.setSizeFull();
		viewLayout.setComponentAlignment(fields, Alignment.MIDDLE_CENTER);
		setCompositionRoot(viewLayout);
	}

	@Override
	public void enter(ViewChangeEvent event) {
	}

	public void loginButtonClicked(Button.ClickEvent event) {
		String username = user.getValue();
		String password = this.password.getValue();
	    
	    UserService userService = UserService.createMockService();
	    
	    User user = userService.find(username, password);
		if (user != null) {
			getSession().setAttribute("username", user.getName());
			getSession().setAttribute("role", user.getRole());
			getSession().setAttribute("avatar", user.getAvatar().getSource());
			getUI().getNavigator().navigateTo(NotebookListView.NAME);
			Notification.show("Logged in", Type.HUMANIZED_MESSAGE);

		} else {
			errorLabel.setVisible(true);
			this.password.setValue("");
            this.password.focus();
		}
	}
}
