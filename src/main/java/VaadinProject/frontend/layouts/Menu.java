package VaadinProject.frontend.layouts;

import com.vaadin.server.ClassResource;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.server.Resource;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import VaadinProject.frontend.MainUI;
import VaadinProject.frontend.views.LoginView;
import VaadinProject.frontend.views.NotebookListView;
import VaadinProject.frontend.views.RegistrationWizardView;

@SuppressWarnings("serial")
public class Menu extends VerticalLayout {

	Button closeButton = new Button("Close", this::hide);
	Image avatar = new Image(null, new ClassResource("/guest.jpg"));
	Label greetings = new Label("Hello, Guest!");
	Button gotoLoginButton = new Button("Login", this::gotoLoginView);
	Button gotoRegistrationButton = new Button("Registration", this::gotoRegistrationView);
	Button logoutButton = new Button("Logout", this::logout);
	VerticalLayout layout = new VerticalLayout();

	public Menu() {
		configureComponents();
		buildLayout();
	}

	private void configureComponents() {

		closeButton.setIcon(FontAwesome.ARROW_LEFT);
		closeButton.setWidth("100%");
		
		avatar.setStyleName("avatar");

		greetings.setWidth(null);
		greetings.setStyleName(ValoTheme.LABEL_LARGE);
		greetings.addStyleName(ValoTheme.LABEL_BOLD);

		gotoLoginButton.setIcon(FontAwesome.SIGN_IN);
		gotoLoginButton.setWidth("100%");
		
		gotoRegistrationButton.setIcon(FontAwesome.USER);
		gotoRegistrationButton.setWidth("100%");

		logoutButton.setIcon(FontAwesome.SIGN_OUT);
		logoutButton.setWidth("100%");

		layout.addComponents(closeButton, avatar, greetings, gotoLoginButton, gotoRegistrationButton, logoutButton);
		layout.setComponentAlignment(avatar, Alignment.MIDDLE_CENTER);
		layout.setComponentAlignment(greetings, Alignment.MIDDLE_CENTER);
		layout.setSpacing(true);
		addStyleName("menu");
		setSizeFull();
	}

	private void buildLayout() {
		addComponents(layout);
		setVisible(false);
	}

	public void show() {
		setElementsVisibility();
		setVisible(true);
	}

	private void setElementsVisibility() {
		
		VaadinSession session = getSession();
		if (session != null && session.getAttribute("username") != null) {
			
			String username = (String) session.getAttribute("username");
			Resource avatarResource = (Resource) session.getAttribute("avatar");
			
			avatar.setSource(avatarResource);
			avatar.setVisible(true);
			greetings.setValue("Hello, " + username + "!");
			gotoLoginButton.setVisible(false);
			gotoRegistrationButton.setVisible(false);
			logoutButton.setVisible(true);
		} else {
			greetings.setValue("Hello, Guest!");
			gotoLoginButton.setVisible(true);
			gotoRegistrationButton.setVisible(true);
			logoutButton.setVisible(false);
		}
	}

	private void hide(Button.ClickEvent event) {
		setVisible(false);
	}

	private void gotoLoginView(Button.ClickEvent event) {
		getUI().getNavigator().navigateTo(LoginView.NAME);
	}
	
	private void gotoRegistrationView(Button.ClickEvent event) {
		getUI().getNavigator().navigateTo(RegistrationWizardView.NAME);
	}

	private void logout(Button.ClickEvent event) {
		getSession().setAttribute("username", null);
		getSession().setAttribute("role", null);
		Notification.show("Logged out", Type.HUMANIZED_MESSAGE);
		getUI().getNavigator().navigateTo(NotebookListView.NAME);
		Page.getCurrent().reload();
	}

	@Override
	public MainUI getUI() {
		return (MainUI) super.getUI();
	}
}
