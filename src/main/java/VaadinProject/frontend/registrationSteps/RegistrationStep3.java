package VaadinProject.frontend.registrationSteps;

import org.vaadin.teemu.wizards.WizardStep;

import com.vaadin.data.validator.EmailValidator;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.themes.ValoTheme;

import VaadinProject.backend.validators.UserValidator;
import VaadinProject.frontend.views.RegistrationWizardView;

public class RegistrationStep3 implements WizardStep {

	private TextField email;
	private Label errorLabel;

	public static final String fieldWidth = "350px";

	@Override
	public String getCaption() {
		return "Enter your email";
	}

	@Override
	public Component getContent() {

		email = new TextField("Email:");
		email.setWidth(fieldWidth);
		email.setRequired(true);
		email.addValidator(new EmailValidator("Invalid email address"));
		email.focus();

		errorLabel = new Label("Email already taken");
		errorLabel.setStyleName(ValoTheme.LABEL_FAILURE);
		errorLabel.setWidth(fieldWidth);
		errorLabel.setVisible(false);

		VerticalLayout content = new VerticalLayout();
		content.setSizeFull();
		content.setMargin(true);

		Label text = getText();
		content.addComponent(text);

		VerticalLayout fields = new VerticalLayout(email, errorLabel);
		fields.setSpacing(true);
		fields.setSizeUndefined();

		content.addComponent(fields);
		content.setComponentAlignment(fields, Alignment.TOP_CENTER);

		return content;
	}

	private Label getText() {
		return new Label("<h1>Enter your email</h1>" + "<p><h2><i>e.g. user@host.com</i></h2></p>", ContentMode.HTML);
	}

	private boolean fieldsValid() {
		return UserValidator.UniqueEmailValidator.isValid((email.getValue()));
	}

	public boolean onAdvance() {
		if (email.getValue() == "") {
			Notification.show("Enter your email address", Type.ERROR_MESSAGE);
			return false;
		}
		if (!email.isValid()) {
			return false;
		}
		if (!fieldsValid()) {
			errorLabel.setVisible(true);
			return false;
		}
		RegistrationWizardView.newUser.setEmail(email.getValue());
		return true;
	}

	public boolean onBack() {
		return true;
	}
}
