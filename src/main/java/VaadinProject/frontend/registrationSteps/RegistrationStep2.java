package VaadinProject.frontend.registrationSteps;

import org.vaadin.teemu.wizards.WizardStep;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import VaadinProject.backend.validators.UserValidator;
import VaadinProject.frontend.views.RegistrationWizardView;

public class RegistrationStep2 implements WizardStep {

	private TextField username;
	private Label errorLabel;

	public static final String fieldWidth = "350px";

	@Override
	public String getCaption() {
		return "Enter your name";
	}

	@Override
	public Component getContent() {

		username = new TextField("Username:");
		username.setWidth(fieldWidth);
		username.setRequired(true);
		username.addValidator(UserValidator.UsernameValidator);
		username.focus();

		errorLabel = new Label("Username already taken");
		errorLabel.setStyleName(ValoTheme.LABEL_FAILURE);
		errorLabel.setWidth(fieldWidth);
		errorLabel.setVisible(false);

		VerticalLayout content = new VerticalLayout();
		content.setSizeFull();
		content.setMargin(true);

		Label text = getText();
		content.addComponent(text);
		
		VerticalLayout fields = new VerticalLayout(username, errorLabel);
		fields.setSpacing(true);
		fields.setSizeUndefined();

		content.addComponent(fields);
		content.setComponentAlignment(fields, Alignment.TOP_CENTER);
		
		return content;
	}

	private Label getText() {
		return new Label(
				"<h1>Enter your name</h1>"
						+ "<p><h2><i>Remember that your username must be <b>at least</b> 3 characters.</i></h2></p>",
				ContentMode.HTML);
	}
	
	private boolean fieldsValid() {
		return UserValidator.UniqueUsernameValidator.isValid((username.getValue()));
	}
		

	public boolean onAdvance() {
		if (username.getValue() == "") {
			Notification.show("Enter your name", Type.ERROR_MESSAGE);
			return false;
		}
		if (!username.isValid()) {
			return false;
		}
		if (!fieldsValid()) {
			errorLabel.setVisible(true);
			return false;
		}
		RegistrationWizardView.newUser.setName(username.getValue());
		return true;
	}

	public boolean onBack() {
		return true;
	}
}
